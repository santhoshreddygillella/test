package test;

import java.util.Scanner;

public class PalindrameTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the number ");
        int n = sc.nextInt();
        int temp = n;
        int revers = 0;

        while (n > 0) {
            int mod = n % 10;
            revers = revers * 10 + mod;
            n = n / 10;

        }
        if (temp == revers) {
            System.out.println(temp + "is palindram");
        } else {
            System.out.println(temp + "is not palindram");
        }
    }

}
