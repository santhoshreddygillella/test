package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FileReaderTestCase1 {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        BufferedReader inputData = new BufferedReader(new FileReader("sunny.csv"));
        String lines;
        HashMap<Integer, Map> dataOuputMap = new <Integer, Map>HashMap();
        Map<Integer, Map> eidkeyMap = new <Integer, Map>LinkedHashMap();
        Map<Integer, Map> firstNameKeyMap = new <Integer, Map>LinkedHashMap();
        Map<Integer, Map> middelNameKeyMap = new <Integer, Map>LinkedHashMap();
        Map<Integer, Map> lastNameKeyMap = new <Integer, Map>LinkedHashMap();
        Map<Integer, Map> locationNameKeyMap = new <Integer, Map>LinkedHashMap();
        Map<Integer, Map> deptNameKeyMap = new <Integer, Map>LinkedHashMap();
        

        List<String> rowsList = new <String>ArrayList();
        while ((lines = inputData.readLine()) != null) {

            rowsList.add(lines);

        }
        String header = rowsList.get(0);
        String[] headerArray = header.split(",");
        for (int i = 1; i < rowsList.size(); i++) {
            String singleRow = rowsList.get(i);
            String[] singleRowChars = singleRow.split(",");

            Map valuesMap = new LinkedHashMap();

            for (int j = 0; j < singleRowChars.length; j++) {
                Map<String, String> eidMap = new <String, String>LinkedHashMap();
                Map<String, String> firstNameMap = new <String, String>LinkedHashMap();
                Map<String, String> middeleNameMap = new <String, String>LinkedHashMap();
                Map<String, String> lastNameMap = new <String, String>LinkedHashMap();
                Map<String, String> locationMap = new <String, String>LinkedHashMap();
                Map<String, String> deptMap = new <String, String>LinkedHashMap();
                valuesMap.put(headerArray[j], singleRowChars[j]);

                switch (j) {
                    case 0:
                        eidMap.put(headerArray[j], singleRowChars[j]);
                        eidkeyMap.put(i, eidMap);
                    case 1:
                        firstNameMap.put(headerArray[j], singleRowChars[j]);
                        firstNameKeyMap.put(i, firstNameMap);
                    case 2:

                        middeleNameMap.put(headerArray[j], singleRowChars[j]);
                        middelNameKeyMap.put(i, middeleNameMap);
                    case 3:
                        lastNameMap.put(headerArray[j], singleRowChars[j]);
                        lastNameKeyMap.put(i, lastNameMap);
                    case 4:
                        locationMap.put(headerArray[j], singleRowChars[j]);
                        locationNameKeyMap.put(i, locationMap);

                    case 5:
                        deptMap.put(headerArray[j], singleRowChars[j]);
                        deptNameKeyMap.put(i, deptMap);

                }

            }
            dataOuputMap.put(i, valuesMap);

        }

        System.out.println(dataOuputMap);
        FileReaderTaskMenu.userMeanu();
        System.out.println("please enter the your choice");
        int userChoice = sc.nextInt();
        switch (userChoice) {
            case 1:
                System.out.println("please enter your row");
                int rowInput = sc.nextInt();
                FileReaderTaskMenu.headerMeanu();

                System.out.println("please enter your HeaderColoum");
                int coloumInput = sc.nextInt();

                switch (coloumInput) {
                    case 1:
                        System.out.println(eidkeyMap.get(rowInput));
                        break;
                    case 2:
                        System.out.println(firstNameKeyMap.get(rowInput));
                        break;
                    case 3:
                        System.out.println(middelNameKeyMap.get(rowInput));
                        break;
                    case 4:
                        System.out.println(lastNameKeyMap.get(rowInput));
                        break;
                    case 5:
                        System.out.println(locationNameKeyMap.get(rowInput));
                        break;
                    case 6:
                        System.out.println(deptNameKeyMap.get(rowInput));
                        break;

                }
                break;

            case 2:
                System.out.println("please enter the rowkey");
                Integer input = (Integer) sc.nextInt();
                if (dataOuputMap.containsKey(input)) {
                    System.out.println(dataOuputMap.get(input));
                    FileReaderTaskMenu.headerMeanu();
                    System.out.println("please enter your option");
                    int coloumInput2 = sc.nextInt();
                    switch (coloumInput2) {
                        case 1:
                            System.out.println(eidkeyMap.get(input));
                            break;
                        case 2:
                            System.out.println(firstNameKeyMap.get(input));
                            break;
                        case 3:
                            System.out.println(middelNameKeyMap.get(input));
                            break;
                        case 4:
                            System.out.println(lastNameKeyMap.get(input));
                            break;
                        case 5:
                            System.out.println(locationNameKeyMap.get(input));
                            break;
                        case 6:
                            System.out.println(deptNameKeyMap.get(input));
                            break;

                    }
                } else {
                    System.out.println("please enter valid key");
                }
        }

    }

}
