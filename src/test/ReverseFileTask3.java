package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReverseFileTask3 {

    public static void main(String[] args) throws IOException {
        File normalDataFile = new File("sandy.csv");
        File reversDataFile = new File("revers.csv");
        System.out.println(reversDataFile.exists());
        reversDataFile.createNewFile();
        List<String> stringDataList = new <String>ArrayList();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(normalDataFile));
        String dataStrings;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(reversDataFile));
        while ((dataStrings = bufferedReader.readLine()) != null) {

            stringDataList.add(dataStrings);

        }
        int t = stringDataList.size();
        System.out.println(t);
        String g = stringDataList.get(0);
        bufferedWriter.write(g);
        bufferedWriter.newLine();

        for (int i = 1; i < stringDataList.size(); i++) {
            String h = stringDataList.get(i);
            String[] s2 = h.split(",");
            int le = s2.length;

            for (String words : s2) {

                StringBuilder stringBuilder = new StringBuilder(words);
                System.out.println(words);
                bufferedWriter.write(stringBuilder.reverse().toString() + ",");
            }
            bufferedWriter.newLine();
        }

        bufferedReader.close();
        bufferedWriter.close();

    }

}
