package test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapTest {

    public static void main(String[] args) {
        Map<String, Integer> m = new <String, Integer>HashMap();
        m.put("santhosh", 1);
        m.put("sunny", 1);
        m.put("bunny", 1);
        System.out.println(m);
        Set j = m.keySet();
        System.out.println(j);
        Collection n = m.values();
        System.out.println(n);
        Set k = m.entrySet();

        System.out.println(k);
        Iterator itr = k.iterator();
        while (itr.hasNext()) {
            Map.Entry c;
            c = (Map.Entry) itr.next();
            System.out.println(c.getValue() + "=" + c.getKey());
            if (c.getKey().equals("santhosh")) {
                c.setValue(1);
            } else if (c.getKey().equals("sunny")) {
                c.setValue(2);
            } else if (c.getKey().equals("bunny")) {
                c.setValue(3);
            }

        }
        System.out.println(m);

        /*while (itr.hasNext()) {
            Map.Entry p;
            p= (Map.Entry) itr.next();
            System.out.println(p.getValue() + "=" + p.getKey());*/
    }
}
