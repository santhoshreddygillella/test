package test;

import java.util.Scanner;

public class StringSortingTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the size of array");
        int k = sc.nextInt();
        String[] s1 = new String[k];
         String temp = "";
        System.out.println("Enter the string elements");
        for (int i = 0; i < s1.length; i++) {
            s1[i] = sc.next();

        }
        for (int i = 0; i < s1.length; i++) {
           
            for (int j = i + 1; j < s1.length - 1-i; j++) {
                if (s1[i].compareTo(s1[j]) > 0) {
                    temp = s1[i];
                    s1[i] = s1[j];
                    s1[j] = temp;

                }
            }

        }
        System.out.println("Sorting Array");
        for (String s : s1) {
            System.out.println(s);
        }
    }

}
