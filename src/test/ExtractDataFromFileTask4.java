package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ExtractDataFromFileTask4 {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        HashMap<Integer, Map> dataOuputMap = new <Integer, Map>HashMap();
        List<String> dataList = new <String>ArrayList();

        File inputFile = new File("sunny.csv");
        BufferedReader inputBufferedReader = new BufferedReader(new FileReader(inputFile));
        String str;
        while ((str = inputBufferedReader.readLine()) != null) {
            dataList.add(str);

        }
        String headingWords = dataList.get(0);
        String[] headingWordsArray = headingWords.split(",");
        for (int i = 1; i < dataList.size(); i++) {
            String rowOfData = dataList.get(i);
            String[] words = rowOfData.split(",");
            List keyList = new ArrayList();

            for (int p = 0; p < 1; p++) {
                keyList.add(words[0]);
            }

            Map valuesList = new LinkedHashMap();
            for (int k = 0; k < words.length; k++) {

                valuesList.put(headingWordsArray[k], words[k]);

            }

            Integer z = Integer.parseInt(keyList.get(0).toString());

            dataOuputMap.put(z, valuesList);

        }
        System.out.println(dataOuputMap);
        System.out.println("please enter the key");
        Integer input = (Integer) sc.nextInt();
        if (dataOuputMap.containsKey(input)) {
            System.out.println(dataOuputMap.get(input));
        } else {
            System.out.println("please enter valid key");
        }
        inputBufferedReader.close();
    }

}
