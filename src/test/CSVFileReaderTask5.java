package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class CSVFileReaderTask5 {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        BufferedReader inputBufferReader = new BufferedReader(new FileReader("FileReader.csv"));

        Set<String> empIdSet = new <String>HashSet();
        Set<String> locationSet = new <String>HashSet();
        HashMap<Integer, Map> DataMap = new <Integer, Map>HashMap();
        List<String> dataInputList = new <String>ArrayList();
        boolean flag =false;

        String DataInput;

        while ((DataInput = inputBufferReader.readLine()) != null) {
            dataInputList.add(DataInput);
            String[] DataInputArray = DataInput.split(",");
            empIdSet.add(DataInputArray[1]);
        }
   
        for (int i = 1; i < empIdSet.size(); i++) {

            List convertSet = new ArrayList(empIdSet);
            String compareData = convertSet.get(i).toString();
            for (int k = 1; k < dataInputList.size(); k++) {
                String list = dataInputList.get(k);

                String[] listDataArray = list.split(",");
                String compareData2 = listDataArray[1];
                if (compareData.equals(compareData2)) {

                    locationSet.add(listDataArray[4]);
                    flag=true;

                }

            }

        }
        System.out.println(locationSet);
        String s = dataInputList.get(0);

        String[] sArray = s.split(",");

        for (int i = 1; i < dataInputList.size(); i++) {
            String c = dataInputList.get(i);

            String[] words = c.split(",");
            List keyList = new ArrayList();

            for (int p = 0; p < 1; p++) {
                keyList.add(words[0]);
            }

            Map valuesList = new LinkedHashMap();

            for (int k = 0; k < words.length; k++) {
                if (k == 4) {
                    if(flag==true){
                    valuesList.put(sArray[k], locationSet.toString());
                    }else{
                        valuesList.put(sArray[k], words[k]);
                    }
                } else {
                    valuesList.put(sArray[k], words[k]);
                }

            }

            System.out.println("list is : " + valuesList);

            Integer z = Integer.parseInt(keyList.get(0).toString());

            DataMap.put(z, valuesList);

        }
        System.out.println("**************");
        System.out.println(DataMap);
        System.out.println("please enter the key");
        Integer input = (Integer) sc.nextInt();
        if (DataMap.containsKey(input)) {
            System.out.println(DataMap.get(input));
        } else {
            System.out.println("please enter valid key");
        }
        inputBufferReader.close();
    }

}
