package test;

import java.util.Scanner;

public class PatternsTask6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("please enter the number of rounds");
        int inputRounds = sc.nextInt();
        System.out.println("please enter the numbers of numbers to view");
        int inputNumber = sc.nextInt();
        int temnum = 0;
        int temp = 2;
        System.out.println("**********************");
        for (int i = 1; i <= inputRounds; i++) {
            for (int j = 1 + temnum; j <= inputNumber; j++) {
                System.out.print(j);
            }
            if (i == temp) {
                for (int k = 1; k <= temnum; k++) {
                    System.out.print(k);
                }
                temp++;
            }
            System.out.println();
            temnum++;
        }
    }

}
