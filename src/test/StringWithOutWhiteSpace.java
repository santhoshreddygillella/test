package test;

import java.util.Scanner;

public class StringWithOutWhiteSpace {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the String with white Spaces s1=");
        String s1 = sc.nextLine();
        char[] s2 = s1.toCharArray();
        String s3 = "";
        for (int i = 0; i < s2.length; i++) {
            if (s2[i] != ' ') {
                s3 = s3 + s2[i];

            }
        }
        System.out.println("String without white Spaces =" + s3);

    }

}
