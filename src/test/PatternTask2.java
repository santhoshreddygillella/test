package test;

public class PatternTask2 {

    public static void main(String[] args) {

        String inputString = "Santhoshreddy";
        char[] charStringArray = inputString.toCharArray();
        int le = charStringArray.length;
        int num = 1;
        for (int k = 0; k < le; k++) {
            for (int i = 0; i < le - k; i++) {
                String outPutString = "";
                for (int j = 0; j < le - k; j++) {
                    if (i == j) {
                        outPutString = outPutString + num;
                    } else {
                        outPutString = outPutString + charStringArray[j];
                    }
                }
                System.out.println(outPutString);
            }
            num++;

        }
    }

}
